import os
import re
import shutil


def main():
    svg_dictionary = {}
    max_file_length = 0
    max_sources_length = 0
    max_names_length = 0
    max_usages_length = 0

    # Gather svg files and path
    for svg_root, svg_dirs, svg_files in os.walk("."):
        for svg_file in svg_files:
            if (
                    svg_file.endswith(".svg")
                    and not svg_root.endswith("old")
                    and not svg_root.endswith("new")
            ):
                if svg_file not in svg_dictionary:
                    # New file
                    svg_dictionary[svg_file] = {}
                    svg_dictionary[svg_file]["sources"] = set()
                    svg_dictionary[svg_file]["names"] = set()
                    svg_dictionary[svg_file]["usages"] = set()

                    max_file_length = max(
                        len(
                            '|<img src="./old/{}" width="48" height="48"/><nobr>{}</nobr>'.format(
                                svg_file, svg_file
                            )
                        ),
                        max_file_length,
                    )

                # Add path to the dictionary
                filepath = os.path.relpath(os.path.join(svg_root, svg_file))
                svg_dictionary[svg_file]["sources"].add(filepath)

    # Gather usages / names
    for usage_root, usage_dirs, usage_files in os.walk("."):
        for usage_file in usage_files:
            if (
                    not usage_file.endswith(".svg")
                    and not usage_root.endswith("old")
                    and not usage_root.endswith("new")
                    and not usage_file.endswith(".md")
                    and (usage_root != "." or not usage_file.endswith(".html"))
            ):
                try:
                    usage_filepath = os.path.join(usage_root, usage_file)

                    with open(usage_filepath, mode="r", encoding="utf-8") as usage:
                        last_lines = ""

                        for usage_line_number, usage_line in enumerate(usage):
                            for svg_file in svg_dictionary:
                                if ("/" + svg_file) in usage_line:
                                    svg_dictionary[svg_file]["usages"].add(
                                        usage_filepath
                                    )

                                    # Search for name
                                    match = re.search(
                                        r"(?:name|label)\s*=\s*\"([^\"]+)\"", usage_line
                                    )
                                    if match is not None:
                                        svg_dictionary[svg_file]["names"].add(
                                            match.group(1)
                                        )
                                    else:
                                        match = re.search(
                                            r"<\s*desc\s*>(.*)<\s*/desc\s*>",
                                            last_line,
                                        )

                                        if match is not None:
                                            svg_dictionary[svg_file]["names"].add(
                                                match.group(1)
                                            )
                                        else:
                                            match = re.search(
                                                r"<\s*name\s*>(.*)<\s*/name\s*>",
                                                last_line,
                                            )

                                            if match is not None:
                                                svg_dictionary[svg_file]["names"].add(
                                                    match.group(1)
                                                )

                            last_line = usage_line

                    for svg_file in svg_dictionary:
                        usage_length = 1
                        for usage_path in svg_dictionary[svg_file]["usages"]:
                            usage_length += len(
                                "<nobr>{}</nobr><BR>".format(usage_path)
                            )

                        max_usages_length = max(usage_length, max_usages_length)

                        name_length = 1
                        for name in svg_dictionary[svg_file]["names"]:
                            name_length += len("<nobr>{}</nobr><BR>".format(name))

                        max_names_length = max(name_length, max_names_length)
                except:
                    pass
                    # Ignore file

    # Define the ones to remove
    to_remove = [
        "GreenMedicine.svg",
        "RemoveMedecine.svg",
        "BlueWait.svg",
        "IDVR_logo.svg",
        "legal.svg",
        "Bracket_white.svg",
        "Ogre_logo.svg",
        "RedRightArrow.svg"
    ]

    # Define duplicates
    duplicates = {
        "BlueHome.svg": "welcome.svg",
        "RedStop.svg": "stop.svg",
        "stop-record.svg": "stop.svg",
        "YellowMaximize.svg": "maximize.svg",
        "YellowFullscreen.svg": "maximize.svg",
        "YellowMinimize.svg": "restore.svg",
        "YellowRestore.svg": "restore.svg",
        "start-record.svg": "RedCircle.svg",
        "CheckButton.svg": "GreenCheck.svg",
        "Apply.svg": "GreenCheck.svg",
        "GreenStart.svg": "play.svg",
        "start-cam.svg": "play.svg",
        "start.svg": "play.svg",
        "Import.svg": "GreenPlus.svg",
        "icon-DICOM-Filtering.svg": "GreenPlus.svg",
        "GreenTracking.svg": "BlueTracking.svg",
        "RedTracking.svg": "BlueTracking.svg",
        "open.svg": "Open.svg",
        "Preview.svg": "BlueMagnifyingGlass.svg",
        "Minus.svg": "RedMinus.svg",
        "Settings.svg": "gear.svg",
        "BlueParameters.svg": "gear.svg",
        "GreenServerConnected.svg": "connection.svg",
        "Network-Receiving.svg": "connection.svg",
        "RedServerDisconnected.svg": "disconnection.svg",
        "Network-Stop-Receiving.svg": "disconnection.svg",
        "TF_grey.svg": "TF.svg",
        "TFintraOp.svg": "TF1.svg",
        "TF_window.svg": "windowing.svg",
        "icon-dump.svg": "Export.svg",
        "ircad.svg": "IRCAD_white_logo.svg",
        "ircad_france_couleur_source.svg": "IRCAD_white_logo.svg",
        "menu.svg": "YellowMenu.svg",
        "check-mark.svg": "GreenCheck.svg",
        "cancel.svg": "RedCross.svg",
        "details-shown.svg": "YellowTopChevron.svg",
        "details-hidden.svg": "YellowBottomChevron.svg",
        "YellowPull.svg": "Pull.svg",
        "pause.svg": "OrangePause.svg",
        "save.svg": "BlueSave.svg",
        "reset.svg": "RedReset.svg",
    }

    # filter non used svg files
    for svg_file in sorted(svg_dictionary):
        if svg_file in to_remove:
            svg_dictionary.pop(svg_file)
        elif (
                svg_dictionary[svg_file]["usages"] is None
                or len(svg_dictionary[svg_file]["usages"]) == 0
        ):
            svg_dictionary.pop(svg_file)

        elif svg_file in duplicates:
            origin = duplicates[svg_file]
            svg_dictionary[origin]["names"].update(svg_dictionary[svg_file]["names"])
            svg_dictionary[origin]["sources"].update(
                svg_dictionary[svg_file]["sources"]
            )
            svg_dictionary[origin]["usages"].update(svg_dictionary[svg_file]["usages"])
            svg_dictionary.pop(svg_file)

    # Add missing files
    svg_dictionary["nowindowing.svg"] = {
        "sources": set(),
        "names": set(),
        "usages": set()
    }

    svg_dictionary["nowindowing.svg"]["sources"].add("./sight/modules/ui/qt/rc/nowindowing.svg")

    svg_dictionary["ramp.png"] = {
        "sources": set(),
        "names": set(),
        "usages": set()
    }

    svg_dictionary["ramp.png"]["sources"].add("./sight/modules/ui/qt/rc/ramp.png")

    svg_dictionary["square.png"] = {
        "sources": set(),
        "names": set(),
        "usages": set()
    }

    svg_dictionary["square.png"]["sources"].add("./sight/modules/ui/qt/rc/square.png")

    # Compute icon column length
    for svg_file in svg_dictionary:
        sources_length = 1
        for path in svg_dictionary[svg_file]["sources"]:
            sources_length += len(
                '<img src="{}" width="48" height="48"/><nobr>{}</nobr><BR>'.format(
                    path, path
                )
            )

        max_sources_length = max(sources_length, max_sources_length)

    print(svg_dictionary)

    # Build merged icon set
    shutil.rmtree("old", ignore_errors=True)
    shutil.rmtree("new", ignore_errors=True)
    os.makedirs("old", exist_ok=True)

    for svg_file in svg_dictionary:
        in_eus = False
        in_disrumpere = False
        in_night = False

        for path in svg_dictionary[svg_file]["sources"]:
            if svg_file in path and "EUS-GPS" in path:
                in_eus = True
                shutil.copy(path, "old")
                break

        if not in_eus:
            for path in svg_dictionary[svg_file]["sources"]:
                if svg_file in path and "disrumpere" in path and "ircad" in path:
                    in_disrumpere = True
                    shutil.copy(path, "old")
                    break

        if not in_eus and not in_disrumpere:
            for path in svg_dictionary[svg_file]["sources"]:
                if svg_file in path and "disrumpere" in path:
                    in_disrumpere = True
                    shutil.copy(path, "old")
                    break

        if not in_eus and not in_disrumpere:
            for path in svg_dictionary[svg_file]["sources"]:
                if svg_file in path and "night" in path:
                    in_night = True
                    shutil.copy(path, "old")
                    break

        if not in_eus and not in_disrumpere and not in_night:
            for path in svg_dictionary[svg_file]["sources"]:
                if svg_file in path and "sight" in path:
                    shutil.copy(path, "old")
                    break

    shutil.copytree(src="old", dst="new", dirs_exist_ok=True)

    manual_description = {
        "2d+t.svg": "Affichage d'une séquence d'image échographique",
        "About.svg": "Affiche des informations à propos de l'application",
        "AddDistance.svg": "Ajout d'une mesure de distance",
        "Axial.svg": "Affichage en vue axiale - voir https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_r%C3%A9f%C3%A9rence_en_anatomie",
        "Axis.svg": "Affichage des axes<BR>centrage de la caméra",
        "Back.svg": "Précédent / en arrière (selon le contexte)",
        "Background.svg": "Mise en arrière plan",
        "BlueFile.svg": "Fichier (selon le contexte)",
        "BlueLoad.svg": "Chargement d'un fichier/dossier (selon le contexte)",
        "BlueMagnifyingGlass.svg": "Recherche",
        "BlueParametersCamera.svg": "Paramètres de la caméra",
        "BlueParametersConnected.svg": "Paramètres de connexion",
        "BlueSave.svg": "Sauvegarde d'un fichier/dossier (selon le contexte)",
        "BlueSwapView.svg": "Échange/basculement de vue/écran",
        "BlueTracking.svg": "Démarre le tracking<BR>Affiche la vue de Debug",
        "CroppingBox.svg": "Affiche l'outil recadrage",
        "Distance.svg": "Affiche l'outil de mesure de distance<BR>Génère des points intermédiaires entre deux points",
        "Export.svg": "Exportation d'un fichier/dossier (selon le contexte)",
        "Extrude.svg": "Extrusion d'une surface",
        "ExtrudeRevert.svg": "Réinitialisation de l'extrusion",
        "Fetus.svg": "Affichage d'une échographie fœtale<BR>Outil de calcul du terme/poids du fœtus (selon le contexte)",
        "Foreground.svg": "Mise au premier-plan",
        "Frontal.svg": "Affichage en vue frontale - voir https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_r%C3%A9f%C3%A9rence_en_anatomie",
        "GreenCheck.svg": "Validation (selon le contexte)",
        "GreenPlus.svg": "Ajout/Plus (selon le contexte)",
        "GreenServerConnected.svg": "Serveur connecté / se connecter à un serveur<BR>Démarre un serveur/démarre un serveur",
        "GreyChessboard.svg": "Affichage de l'échiquier de calibration/régler la taille de l'échiquier de calibration",
        "HideDistance.svg": "Masque les mesures de distance",
        "IDVR.svg": "Fait un trou dans l'image pour afficher les trucs importants",
        "IDVR_logo.svg": "Logo de l'application de démonstration IDVR",
        "ImportSession.svg": "Importation d'un fichier/dossier ou d'une session de travail (selon le contexte)",
        "JugularNotch.svg": "Sélection de la fourchette sternale (point anatomique à sélectionner)",
        "Kidney.svg": "Activités liées aux reins",
        "Layers.svg": "Affiche de la vue negatoscope / Sélection des coupes (selon le contexte)",
        "Light.svg": "Affiche les paramètres de la lumière",
        "Liver.svg": "Activités liées au foie",
        "LowerLandmark.svg": "Sélection du point anatomique inférieur",
        "Oesophagus.svg": "Sélection des points de l'œsophage pour le recalage",
        "Open.svg": "Ouverture d'un fichier/dossier/session (selon le contexte)",
        "OrangeLocalization.svg": "choix du mode de localisation",
        "OrangeLoop.svg": "Boucle de lecture",
        "Pancreas.svg": "Sélection de points du pancréas pour faire un trou dans l'image pour le voir (IDVR)",
        "Perspective.svg": "Affichage en perspective (hors axe axial/frontal/sagittal)",
        "Points.svg": "Affiche un nuage de points",
        "Pull.svg": "Récupérer une image médicale du serveur (PACS)",
        "Push.svg": "Envoyer une image médicale vers le serveur (PACS)",
        "RedCircle.svg": "Enregistrer vidéo / volume / image, (selon le contexte)",
        "RedCross.svg": "Abandon, quitter (selon le contexte)",
        "RedMinus.svg": "enlever/supprimer/diminuer (selon le contexte)",
        "RedReset.svg": "Réinitialisation des paramètres (selon le contexte)",
        "RedResetTracking.svg": "Réinitialisation du tracking",
        "RedTrashBin.svg": "Effacer / tout effacer",
        "RedUntracking.svg": "Arrêter le tracking",
        "RegistrationCTToEM.svg": "Recalage (https://fr.wikipedia.org/wiki/Recalage_d%27images) du monde image scanner CT vers le monde électromagnétique (capteur)",
        "RegistrationCTUS.svg": "Recalage du monde image scanner CT vers le monde échographique (capteur)",
        "RegistrationPoints.svg": "Recalage par points",
        "RemoveDistance.svg": "Suppression d'une mesure de distance",
        "RemoveLandmark.svg": "Suppression d'un point anatomique",
        "ResetCroppingBox.svg": "Réinitialisation de l'outil recadrage",
        "ResetRegistrationCTUS.svg": "Réinitialisation du recalage CT-US",
        "Sagittal.svg": "Affichage en vue sagittale - voir https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_r%C3%A9f%C3%A9rence_en_anatomie",
        "ShowDistance.svg": "Affiche les mesures de distance",
        "Snow_flake.svg": "Figer l'image",
        "Sternum.svg": "Affiche l'outil de sélection des points anatomiques du sternum",
        "SternumPath.svg": "Sélection des points anatomiques du sternum",
        "Stomach.svg": "Affiche l'outil de gestion des organes",
        "TF.svg": "Sélectionne la fonction de transfert (choix des couleurs)",
        "TF1.svg": "Affiche l'éditeur de fonction de transfert 1 (volume)",
        "TF2.svg": "Affiche l'éditeur de fonction de transfert 2 ()",
        "Trackball.svg": "Affiche les sélections d'orientation de la caméra (axiale, sagittale, frontale, perspective)",
        "US.svg": "sonde échographique / image échographie / paramètres de l'image échographique (selon le contexte)",
        "US_Auto.svg": "auto-calibration de la sonde échographie",
        "US_probe_settings.svg": "paramètres de la sonde échographique",
        "Ultrasound.svg": "Affichage de la sonde / image échographique",
        "UpperLandmark.svg": "Sélection du point anatomique supérieur",
        "Volume.svg": "Affichage en 3d / des volumes / reconstruction d'un volume (selon le contexte)",
        "XiphoidProcess.svg": "Sélection du processus xiphoïde (point anatomique à sélectionner)",
        "YellowAddLandmarks.svg": "Ajout de points anatomiques",
        "YellowCamera.svg": "Calibration / Affiche les paramètres de calibration la caméra",
        "YellowDistortion.svg": "Active/désactive la correction de la distorsion de la caméra",
        "YellowLandmarks.svg": "Affiche les outils d'ajout et de suppression des points anatomiques",
        "YellowLeftChevron.svg": "Affiche la vue précédente / affichage d'un panneau supplémentaire à gauche / fermeture du panneau ouvert à droite",
        "YellowMenu.svg": "Affiche un menu / panneau de contrôle (selon le contexte)",
        "YellowMovieRecord.svg": "Enregistrement d'une vidéo",
        "YellowPhoto.svg": "Enregistrement d'une image / capture d'écran",
        "YellowPhotoRecord.svg": "Enregistrement d'une séquence d'images / vidéos",
        "YellowRemoveLandmarks.svg": "Suppression de points anatomiques",
        "YellowRightChevron.svg": "Affiche la vue suivante / affichage d'un panneau supplémentaire à droite / fermeture du panneau ouvert à gauche",
        "YellowTarget.svg": "Change la cible courante",
        "YellowUndistortion.svg": "Active/désactive la distorsion de la caméra",
        "calibrationActivity.svg": "Affiche l'activité de calibration",
        "connection.svg": "Connexion à un serveur / tracking / robot (selon le contexte)",
        "contact-off.svg": "monte la sonde (enlève le contact avec le patient)",
        "contact-on.svg": "descend la sonde (met en contact avec le patient)",
        "crossHair.svg": "Guidage de l'aiguille (cible)",
        "cutVolume.svg": "retire de l'image les parties non intéressantes (dessin à main levée)",
        "disconnection.svg": "Déconnexion d'un serveur / tracking / robot (selon le contexte)",
        "estop.svg": "Arrêt d'urgence",
        "exam-mode.svg": "Mode examen",
        "freedrive-mode.svg": "Mode libre (déplacement du bras robotique à la main)",
        "gear.svg": "Paramètres / préférences / configuration (selon le contexte)",
        "hide2d+t.svg": "Masque les séquences d'images échographiques 2d",
        "hideNegato.svg": "Masque la vue negatoscope / les coupes",
        "hideVolume.svg": "Masque les volumes",
        "icon-Anonymization.svg": "Anonymisation (image / dossier patient.. ",
        "lock.svg": "Verrouillage du robot / de l'interface",
        "maximize.svg": "Agrandissement de la fenêtre",
        "nnFetusSegmentation.svg": "Démarre l'extraction du fœtus dans l'image<BR>Calcul du poids du fœtus",
        "nnKidneyBoudarySegmentation.svg": "Démarre le détourage des reins dans l'image",
        "nnKidneyBoudarySegmentationStop.svg": "Arrête le détourage des reins dans l'image",
        "nnKidneyDetection.svg": "Démarre la détection des reins dans l'image",
        "nnKidneyDetectionStop.svg": "Arrête la détection des reins dans l'image",
        "nnKidneySegmentation.svg": "Démarre l'extraction des reins dans l'image",
        "nnKidneySegmentationStop.svg": "Arrête l'extraction des reins dans l'image",
        "nnLiverSegmentation.svg": "Démarre l'extraction du foie dans l'image",
        "nnLiverSegmentationStop.svg": "Arrête l'extraction du foie dans l'image",
        "nnTumorDetection.svg": "Démarre la détection des tumeurs dans l'image",
        "nnTumorDetectionStop.svg": "Arrête la détection des tumeurs dans l'image",
        "nnTumorSelection.svg": "Sélection des tumeurs dans l'image",
        "opacity.svg": "Modifie l'opacité de l'image",
        "parking.svg": "Parking du robot (mise en sécurité)",
        "play.svg": "Lecture d'une vidéo / démarrage (selon le contexte)",
        "pterobot.svg": "Application Pterobot",
        "pteropus.svg": "Application Pteropus",
        "record2d+t.svg": "Enregistrement d'une séquence d'image échographique",
        "restore.svg": "Restauration de la taille de la fenêtre / vue",
        "revertCutVolume.svg": "Réinitialisation de l'image avec des parties non intéressantes",
        "safety-check-reset.svg": "Réinitialisation des vérifications de sécurité / calibration du robot",
        "safety-check.svg": "Vérifications de sécurité / calibration du robot",
        "selector.svg": "Sélection d'une image parmi une liste d'images",
        "set-welcome.svg": "ajoute un point de référence pour le robot",
        "show2d+t.svg": "Affiche les séquences d'images échographiques 2d",
        "showNegato.svg": "Affiche la vue negatoscope / les coupes",
        "showVolume.svg": "Affiche les volumes",
        "sight_logo.svg": "Logo de la librairie Sight",
        "stop.svg": "Arrêt d'une vidéo / d'un enregistrement / arrêt (selon le contexte)",
        "targeting-off.svg": "Arrête le mode ciblage du robot",
        "targeting-on.svg": "Démarre le mode ciblage du robot",
        "undoCutVolume.svg": "Annule la dernière action de retrait de partie non intéressante de l'image",
        "unlock.svg": "Déverrouillage du robot / de l'interface",
        "usMouseCT.svg": "Active le sélection de la coupe à l'aide de la sonde échographique",
        "usMouseCTReset.svg": "Réinitialise la sélection de la coupe à l'aide de la sonde échographique",
        "usrecorder.svg": "Application USRecorder",
        "validate-target.svg": "Valide la cible",
        "welcome.svg": "Affiche la vue d'accueil / retour à la position d'accueil du robot",
        "windowing.svg": "Modifie le contraste / luminosité de l'image",
        "GreenCopyFile.svg": "Copie (selon le contexte)",
        "GreenNewFile.svg": "Nouveau fichier (selon le contexte)",
        "EZEUS.svg": "Logo de l'application EZ-EUS",
        "ImageSeries.svg": "Image médicale",
        "ModelSeries.svg": "Modèle 3D médical",
        "RedDeleteFile.svg": "Suppression d'un fichier",
        "RedResetFile.svg": "Réinitialisation d'un fichier",
        "YellowPen.svg": "Edition du texte / outil de dessin",
        "YellowRenameFile.svg": "Renommage d'un fichier / objet (selon le contexte)",
        "YellowTopChevron.svg": "Cache les détails",
        "YellowBottomChevron.svg": "Affiche les détails",
        "up.svg": "Monter",
        "down.svg": "Descendre",
        "left.svg": "Déplacer à Gauche",
        "right.svg": "Déplacer à Droite",
        "rotateLeft.svg": "Rotation vers la gauche",
        "rotateRight.svg": "Rotation vers la droite",
        "ramp.png": "Créer une courbe en forme de 'rampe'",
        "square.png": "Créer une courbe en forme de 'carré'",
        "nowindowing.svg": "Réinitialise le contraste / luminosité de l'image",
    }

    # Build the markdown file
    with open("readme_lite.md", mode="w", encoding="utf-8") as readme_lite:
        with open("readme_full.md", mode="w", encoding="utf-8") as readme_full:
            # Compute column length
            files_column = "| Fichiers ".ljust(max_file_length)
            description_column = "| Description ".ljust(max_names_length)
            sources_column = "| Sources ".ljust(max_sources_length)
            names_column = "| Noms ".ljust(max_names_length)
            usages_column = "| Utilisation ".ljust(max_usages_length)

            # Create header
            header = files_column + description_column + sources_column + names_column + usages_column + "|\n"
            header += "|".ljust(len(files_column), "-")
            header += "|".ljust(len(description_column), "-")
            header += "|".ljust(len(sources_column), "-")
            header += "|".ljust(len(names_column), "-")
            header += "|".ljust(len(usages_column), "-")
            header += "|\n"
            readme_full.write(header)

            header_lite = files_column + description_column + "|\n"
            header_lite += "|".ljust(len(files_column), "-")
            header_lite += "|".ljust(len(description_column), "-")
            header_lite += "|\n"
            readme_lite.write(header_lite)

            # Fill table
            for svg_file in sorted(svg_dictionary):
                file = (
                    '|<img src="./old/{}" width="48" height="48"/><nobr>{}</nobr>'.format(
                        svg_file, svg_file
                    ).ljust(max_file_length)
                )

                name_list = sorted(svg_dictionary[svg_file]["names"])

                descriptions = "|"
                if svg_file in manual_description:
                    descriptions += manual_description[svg_file]
                elif len(name_list) > 0:
                    descriptions += name_list[0]

                descriptions = descriptions.ljust(max_names_length)

                sources = "|"
                for svg_path in sorted(svg_dictionary[svg_file]["sources"]):
                    sources += (
                        '<img src="{}" width="48" height="48"/><nobr>{}</nobr><BR>'.format(
                            svg_path, svg_path
                        )
                    )

                sources = sources.ljust(max_sources_length)

                names = "|"
                for svg_name in name_list:
                    names += "<nobr>{}</nobr><BR>".format(svg_name)

                names = names.ljust(max_names_length)

                usages = "|"
                for usage_path in sorted(svg_dictionary[svg_file]["usages"]):
                    usages += "<nobr>{}</nobr><BR>".format(usage_path)

                usages = usages.ljust(max_usages_length)

                line = file + descriptions + sources + names + usages + "|\n"
                readme_full.write(line)

                line_lite = file + descriptions + "|\n"
                readme_lite.write(line_lite)


if __name__ == "__main__":
    main()
